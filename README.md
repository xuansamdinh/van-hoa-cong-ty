# README #

Văn Hóa Công Ty - Hồng Hà - Tháng 01/2016.

### Kho này nhằm mục đích gì? ###

- Kho được khởi tạo nhằm mục đích lưu trữ công việc chỉnh sửa, bổ sung bản xuất bản của Quy ước về Văn Hóa Công Ty - Hồng Hà.

### Kho này chứa gì? ###

Kho bao gồm:

- *.tex files: Bảng quy ước được soạn thảo sử dụng LaTeX.
- *.mm files: Mindmap phác thảo nội dung đã nhắc ở trên.

### Kho này cho ai? ###

- Tất cả những ai có hoặc phải quan tâm đến bản quy ước.
- Tất cả những ai có ý kiến đóng góp, bổ sung, theo dõi bản quy ước.

### Downloads ###

- Bản pdf hoàn thành có thể tải về tại mục [Downloads](https://bitbucket.org/xuansamdinh/van-hoa-cong-ty/downloads) của kho này.