<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1453905483576" ID="ID_1395680189" MODIFIED="1453994093436" STYLE="fork">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n H&#243;a C&#244;ng Ty
    </p>
  </body>
</html></richcontent>
<edge COLOR="#006666" STYLE="sharp_linear"/>
<font BOLD="true" NAME="Resagnicto" SIZE="30"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1453905705360" ID="ID_1008152907" MODIFIED="1453994344597" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n h&#243;a
    </p>
    <p style="text-align: center">
      giao ti&#7871;p
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font BOLD="true" NAME="UTM Times" SIZE="24"/>
<node COLOR="#00b439" CREATED="1453906867451" ID="ID_900278966" MODIFIED="1453993475931">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n h&#243;a &#7913;ng x&#7917;
    </p>
    <p style="text-align: center">
      trong C&#244;ng ty
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="UTM Times" SIZE="22"/>
<node COLOR="#990000" CREATED="1453907124218" ID="ID_1418592510" MODIFIED="1453993475942" TEXT="Ch&#xe0;o h&#x1ecf;i khi g&#x1eb7;p m&#x1eb7;t">
<font BOLD="true" NAME="UTM Times" SIZE="20"/>
<node COLOR="#111111" CREATED="1453907164325" ID="ID_1317596622" MODIFIED="1453993475946" STYLE="fork" TEXT="&#xc1;nh m&#x1eaf;t, n&#xe9;t m&#x1eb7;t v&#xe0; c&#x1eed; ch&#x1ec9; ch&#xe0;o h&#x1ecf;i th&#x1ec3; hi&#x1ec7;n s&#x1ef1; th&#xe2;n thi&#x1ec7;n">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907189180" ID="ID_1144488487" MODIFIED="1453993475952" STYLE="fork" TEXT="Nh&#xe2;n vi&#xea;n ch&#xe0;o nh&#xe0; qu&#x1ea3;n l&#xfd;, c&#x1ea5;p d&#x1b0;&#x1edb;i ch&#xe0;o c&#x1ea5;p tr&#xea;n v&#xe0; ng&#x1b0;&#x1ee3;c l&#x1ea1;i">
<font NAME="UTM Times" SIZE="18"/>
</node>
</node>
<node COLOR="#990000" CREATED="1453907133799" ID="ID_1549856021" MODIFIED="1453993475957" TEXT="L&#x1edd;i l&#x1ebd; khi giao ti&#x1ebf;p">
<font BOLD="true" NAME="UTM Times" SIZE="20"/>
<node COLOR="#111111" CREATED="1453907202815" ID="ID_1008252752" MODIFIED="1453993475959" STYLE="fork" TEXT="Th&#xe1;i &#x111;&#x1ed9; c&#x1edf;i m&#x1edf;, vui v&#x1ebb;">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907282540" ID="ID_1682405467" MODIFIED="1453993475962" STYLE="fork" TEXT="Kh&#xf4;ng v&#x103;ng t&#x1ee5;c, ch&#x1eed;i th&#x1ec1;, m&#x1ec9;a mai &#x111;&#x1ed3;ng nghi&#x1ec7;p, n&#xf3;i x&#x1ea5;u vu kh&#x1ed1;ng ng&#x1b0;&#x1edd;i kh&#xe1;c">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907290545" ID="ID_510028208" MODIFIED="1453993475965" STYLE="fork" TEXT="Kh&#xf4;ng c&#xe3;i nhau, kh&#xf4;ng &#x111;&#x1ead;p b&#xe0;n, ch&#x1ec9; tay khi n&#xf3;i chuy&#x1ec7;n">
<font NAME="UTM Times" SIZE="18"/>
</node>
</node>
<node COLOR="#990000" CREATED="1453907362004" ID="ID_851377176" MODIFIED="1453993475967" TEXT="X&#xe2;y d&#x1ef1;ng l&#x1ed1;i &#x1ee9;ng x&#x1eed; l&#xe0;nh m&#x1ea1;nh">
<font BOLD="true" NAME="UTM Times" SIZE="20"/>
<node COLOR="#111111" CREATED="1453907383361" ID="ID_46498070" MODIFIED="1453993475970" STYLE="fork" TEXT="Tr&#xea;n tinh th&#x1ea7;n c&#x1ed9;ng t&#xe1;c, &#x111;o&#xe0;n k&#x1ebf;t v&#xe0; chia s&#x1ebb;">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907403622" ID="ID_1070993548" MODIFIED="1453993475971" STYLE="fork" TEXT="V&#xec; m&#xf4;i tr&#x1b0;&#x1edd;ng v&#x103;n h&#xf3;a chung c&#x1ee7;a C&#xf4;ng ty">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907418983" ID="ID_500397993" MODIFIED="1453993475973" STYLE="fork" TEXT="C&#xf9;ng ho&#xe0;n th&#xe0;nh nh&#x1eef;ng m&#x1ee5;c ti&#xea;u chung">
<font NAME="UTM Times" SIZE="18"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1453906879441" ID="ID_425465250" MODIFIED="1453993475974">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n h&#243;a &#7913;ng x&#7917; v&#7899;i
    </p>
    <p style="text-align: center">
      kh&#225;ch h&#224;ng v&#224; &#273;&#7889;i t&#225;c
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="UTM Times" SIZE="22"/>
<node COLOR="#990000" CREATED="1453907452685" ID="ID_176180998" MODIFIED="1453993475983" TEXT="Th&#xe1;i &#x111;&#x1ed9;">
<font BOLD="true" NAME="UTM Times" SIZE="20"/>
<node COLOR="#111111" CREATED="1453907554012" ID="ID_181745533" MODIFIED="1453993475986" STYLE="fork" TEXT="L&#x1ecb;ch s&#x1ef1;, nh&#xe3; nh&#x1eb7;n, l&#x1eaf;ng nghe">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907594565" ID="ID_202675826" MODIFIED="1453993475988" STYLE="fork" TEXT="Chia s&#x1ebb; v&#xe0; t&#x1b0; v&#x1ea5;n c&#xe1;c th&#x1eaf;c m&#x1eaf;c c&#x1ee7;a kh&#xe1;ch h&#xe0;ng m&#x1ed9;t c&#xe1;ch chu &#x111;&#xe1;o">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907636202" ID="ID_1964743371" MODIFIED="1453993475989" STYLE="fork" TEXT="Thi&#x1ec7;n ch&#xed;, h&#x1ee3;p t&#xe1;c v&#xe0; t&#xf4;n tr&#x1ecd;ng l&#x1ee3;i &#xed;ch c&#x1ee7;a hai b&#xea;n">
<font NAME="UTM Times" SIZE="18"/>
</node>
</node>
<node COLOR="#990000" CREATED="1453907468965" ID="ID_188553824" MODIFIED="1453993475991" TEXT="Ng&#xf4;n ng&#x1eef;">
<font BOLD="true" NAME="UTM Times" SIZE="20"/>
<node COLOR="#111111" CREATED="1453907795168" ID="ID_1888076834" MODIFIED="1453993475993" STYLE="fork" TEXT="C&#x1edf;i m&#x1edf;, th&#xe2;n thi&#x1ec7;n nh&#x1eb1;m t&#x1ea1;o c&#x1ea7;u n&#x1ed1;i, s&#x1ef1; g&#x1ea7;n g&#x169;i gi&#x1eef;a &#x111;&#xf4;i b&#xea;n">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907839312" ID="ID_238304185" MODIFIED="1453993475995" STYLE="fork" TEXT="Lu&#xf4;n th&#x1ec3; hi&#x1ec7;n s&#x1ef1; t&#xf4;n tr&#x1ecd;ng, l&#x1ecb;ch s&#x1ef1;, b&#xec;nh &#x111;&#x1eb3;ng">
<font NAME="UTM Times" SIZE="18"/>
</node>
</node>
<node COLOR="#990000" CREATED="1453907456718" ID="ID_663723908" MODIFIED="1453993475997" TEXT="T&#xe1;c phong">
<font BOLD="true" NAME="UTM Times" SIZE="20"/>
<node COLOR="#111111" CREATED="1453907865202" ID="ID_116817956" MODIFIED="1453993475999" STYLE="bubble" TEXT="T&#x1ead;n t&#xec;nh, chu &#x111;&#xe1;o">
<font BOLD="true" NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907889311" ID="ID_314554063" MODIFIED="1453993530660" STYLE="fork" TEXT="Ti&#x1ebf;p nh&#x1ead;n, x&#x1eed; l&#xfd; c&#xe1;c th&#xf4;ng tin, y&#xea;u c&#x1ea7;u c&#x1ee7;a kh&#xe1;ch h&#xe0;ng m&#x1ed9;t c&#xe1;ch nhanh ch&#xf3;ng v&#xe0; linh ho&#x1ea1;t">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907896320" ID="ID_18925896" MODIFIED="1453993507567" STYLE="fork" TEXT="Gi&#x1ea3;i quy&#x1ebf;t c&#xf4;ng vi&#x1ec7;c &#x111;&#xfa;ng h&#x1eb9;n, &#x111;&#xfa;ng n&#x1ed9;i dung, &#x111;&#x1ea7;y &#x111;&#x1ee7;, r&#xf5; r&#xe0;ng c&#xe1;c v&#x1ea5;n &#x111;&#x1ec1; khi l&#xe0;m vi&#x1ec7;c">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907917358" ID="ID_870317517" MODIFIED="1453993476021" STYLE="fork" TEXT="Kh&#xf4;ng bao gi&#x1edd; m&#x1ea5;t b&#xec;nh t&#x129;nh khi gi&#x1ea3;i quy&#x1ebf;t c&#xf4;ng vi&#x1ec7;c v&#x1edb;i kh&#xe1;ch h&#xe0;ng, &#x111;&#x1ed1;i t&#xe1;c">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453907929065" ID="ID_25774748" MODIFIED="1453993476023" STYLE="fork" TEXT="&#x110;&#x1ed3;ng ph&#x1ee5;c v&#xe0; &#x111;eo th&#x1ebb; nghi&#x1ec7;p v&#x1ee5; theo quy &#x111;&#x1ecb;nh trong th&#x1edd;i gian l&#xe0;m vi&#x1ec7;c v&#xe0; giao d&#x1ecb;ch v&#x1edb;i kh&#xe1;ch h&#xe0;ng, &#x111;&#x1ed1;i t&#xe1;c">
<font NAME="UTM Times" SIZE="18"/>
</node>
</node>
<node COLOR="#990000" CREATED="1453908458721" ID="ID_1016588568" MODIFIED="1453993476030">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      Qua h&#7879;
    </p>
    <p style="text-align: center">
      th&#7889;ng tho&#7841;i
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="UTM Times" SIZE="20"/>
<node COLOR="#111111" CREATED="1453908607768" ID="ID_474846845" MODIFIED="1453993476037" STYLE="fork" TEXT="L&#x1ecb;ch s&#x1ef1;, h&#xf2;a nh&#xe3;">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453908625720" ID="ID_360329809" MODIFIED="1453993476039" STYLE="fork" TEXT="Ghi ch&#xe9;p &#x111;&#x1ea7;y &#x111;&#x1ee7; c&#xe1;c n&#x1ed9;i dung y&#xea;u c&#x1ea7;u c&#x1ee7;a kh&#xe1;ch">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453908644892" ID="ID_1828122928" MODIFIED="1453993476041" STYLE="fork" TEXT="Tr&#x1ea3; l&#x1edd;i theo ph&#x1ea1;m vi tr&#xe1;ch nhi&#x1ec7;m &#x111;&#x1b0;&#x1ee3;c giao v&#x1edb;i kho&#x1ea3;ng th&#x1edd;i gian ng&#x1eaf;n nh&#x1ea5;t c&#xf3; th&#x1ec3;">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453908681937" ID="ID_683015135" MODIFIED="1453993476046" TEXT="N&#xf3;i ng&#x1eaf;n g&#x1ecd;n, r&#xf5; r&#xe0;ng, kh&#xf4;ng &#x1ea3;nh h&#x1b0;&#x1edf;ng &#x111;&#x1ebf;n nh&#x1eef;ng ng&#x1b0;&#x1edd;i xung quanh">
<font NAME="UTM Times" SIZE="18"/>
</node>
<node COLOR="#111111" CREATED="1453908691396" ID="ID_886793289" MODIFIED="1453993476047" STYLE="fork" TEXT="Gi&#x1ecd;ng n&#xf3;i vui v&#x1ebb;, t&#xed;ch c&#x1ef1;c th&#x1ec3; hi&#x1ec7;n s&#x1ef1; s&#x1eb5;n s&#xe0;ng gi&#xfa;p &#x111;&#x1ee1;">
<font NAME="UTM Times" SIZE="18"/>
</node>
</node>
<node COLOR="#990000" CREATED="1453908731170" ID="ID_187948904" MODIFIED="1453993476050" TEXT="Qua Email">
<font BOLD="true" NAME="UTM Times" SIZE="20"/>
<node COLOR="#111111" CREATED="1453908743266" ID="ID_868663239" MODIFIED="1453993476051" STYLE="fork" TEXT="&#x110;&#x1ec1; c&#x1ead;p &#x1edf; m&#x1ee5;c 3">
<linktarget COLOR="#ff0099" DESTINATION="ID_868663239" ENDARROW="None" ENDINCLINATION="404;0;" ID="Arrow_ID_24088649" SOURCE="ID_1925684987" STARTARROW="Default" STARTINCLINATION="404;0;"/>
<font BOLD="true" NAME="UTM Times" SIZE="18"/>
</node>
</node>
<node COLOR="#990000" CREATED="1453908849004" ID="ID_1549603424" MODIFIED="1453993476054" STYLE="fork" TEXT="Gi&#x1eef; uy t&#xed;n, &#x111;&#x1ea3;m b&#x1ea3;o y&#xea;u c&#x1ea7;u c&#x1ee7;a kh&#xe1;ch">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453908864977" ID="ID_1554134850" MODIFIED="1453993476063" STYLE="fork" TEXT="T&#x1ea1;o ni&#x1ec1;m tin, ch&#x1ee9;ng minh s&#x1ef1; l&#x1ef1;a ch&#x1ecd;n c&#x1ee7;a kh&#xe1;ch h&#xe0;ng &#x111;&#x1ed1;i v&#x1edb;i C&#xf4;ng ty l&#xe0; &#x111;&#xfa;ng &#x111;&#x1eaf;n">
<font NAME="UTM Times" SIZE="20"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1453905724778" FOLDED="true" ID="ID_243337771" MODIFIED="1453994298221" POSITION="left" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n h&#243;a
    </p>
    <p style="text-align: center">
      l&#224;m vi&#7879;c
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font BOLD="true" NAME="UTM Times" SIZE="24"/>
<node COLOR="#00b439" CREATED="1453908903523" ID="ID_1822894128" MODIFIED="1453993476082" STYLE="fork" TEXT="Ph&#x1b0;&#x1a1;ng Ch&#xe2;m: L&#xe0;m Vi&#x1ec7;c T&#x1ef1; Gi&#xe1;c, Quan T&#xe2;m &#x110;&#x1ebf;n Hi&#x1ec7;u Qu&#x1ea3; C&#xf4;ng Vi&#x1ec7;c, Gi&#x1eef; Ch&#x1eef; T&#xed;n">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
</node>
<node COLOR="#00b439" CREATED="1453906901191" ID="ID_1665249589" MODIFIED="1453993476098" TEXT="Tr&#xe1;ch nhi&#x1ec7;m">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
<node COLOR="#990000" CREATED="1453908990863" ID="ID_368366000" MODIFIED="1453993476100" STYLE="fork" TEXT="Th&#xe1;i &#x111;&#x1ed9; t&#xed;ch c&#x1ef1;c, l&#xe0;m vi&#x1ec7;c c&#xf3; tr&#xe1;ch nhi&#x1ec7;m">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909019933" ID="ID_1141583236" MODIFIED="1453993476103" STYLE="fork" TEXT="&#x110;&#xfa;ng quy tr&#xec;nh, quy &#x111;&#x1ecb;nh c&#x1ee7;a c&#xf4;ng ty">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909048125" ID="ID_561599264" MODIFIED="1453993476107" STYLE="fork" TEXT="T&#x1ef1; gi&#xe1;c nh&#x1ead;n khuy&#x1ebf;t &#x111;i&#x1ec3;m">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909059620" ID="ID_1895156764" MODIFIED="1453993476110" STYLE="fork" TEXT="T&#xed;ch c&#x1ef1;c chia s&#x1ebb; kinh nghi&#x1ec7;m l&#xe0;m vi&#x1ec7;c">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909098453" ID="ID_1168848929" MODIFIED="1453993476111" STYLE="fork" TEXT="T&#x1ef1; gi&#xe1;c l&#xe0;m vi&#x1ec7;c, tu&#xe2;n th&#x1ee7; k&#x1ef7; lu&#x1ead;t trong lao &#x111;&#x1ed9;ng">
<font NAME="UTM Times" SIZE="20"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1453906909071" ID="ID_656553761" MODIFIED="1453993476113" TEXT="&#x110;o&#xe0;n k&#x1ebf;t">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
<node COLOR="#990000" CREATED="1453909583901" ID="ID_44801332" MODIFIED="1453993476116" STYLE="fork" TEXT="C&#xe1;c th&#xe0;nh vi&#xea;n chung m&#x1ed9;t ch&#xed; h&#x1b0;&#x1edb;ng x&#xe2;y d&#x1ef1;ng v&#xe0; ph&#xe1;t tri&#x1ec3;n c&#xf4;ng ty &#x111;&#x1ec3; ph&#xe1;t tri&#x1ec3;n ngh&#x1ec1; nghi&#x1ec7;p">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909649405" ID="ID_1080211935" MODIFIED="1453993476123" STYLE="fork" TEXT="S&#x1ef1; ph&#xe1;t tri&#x1ec3;n c&#x1ee7;a C&#xf4;ng ty g&#x1eaf;n li&#x1ec1;n v&#x1edb;i s&#x1ef1; ph&#xe1;t tri&#x1ec3;n c&#x1ee7;a t&#x1eeb;ng th&#xe0;nh vi&#xea;n">
<font NAME="UTM Times" SIZE="20"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1453906915384" ID="ID_1980612203" MODIFIED="1453993476125" TEXT="K&#x1ef7; lu&#x1ead;t">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
</node>
<node COLOR="#00b439" CREATED="1453906923912" ID="ID_1022385663" MODIFIED="1453993476129" TEXT="Trung th&#x1ef1;c">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
<node COLOR="#990000" CREATED="1453909244414" ID="ID_1926923974" MODIFIED="1453993476130" STYLE="fork" TEXT=" Kh&#xf4;ng t&#x1b0; l&#x1ee3;i c&#xe1; nh&#xe2;n">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909255150" ID="ID_239695824" MODIFIED="1453993605673" STYLE="fork">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Trung th&#7921;c v&#7899;i &#273;&#7891;ng nghi&#7879;p, c&#7845;p qu&#7843;n l&#253;,
    </p>
    <p>
      c&#361;ng nh&#432; kh&#225;ch h&#224;ng, &#273;&#7889;i t&#225;c
    </p>
  </body>
</html>
</richcontent>
<font NAME="UTM Times" SIZE="20"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1453906933331" ID="ID_935890133" MODIFIED="1453993476140" TEXT="Tinh th&#x1ea7;n ph&#x1ea5;n &#x111;&#x1ea5;u">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
<node COLOR="#990000" CREATED="1453909353747" ID="ID_734204435" MODIFIED="1453993476143" STYLE="fork" TEXT="Tinh th&#x1ea7;n c&#x1ea7;u ti&#x1ebf;n trong c&#xf4;ng vi&#x1ec7;c">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909370050" ID="ID_701439245" MODIFIED="1453993476144" STYLE="fork" TEXT="Ho&#xe0;n thi&#x1ec7;n, n&#xe2;ng cao k&#x1ef9; n&#x103;ng nghi&#x1ec7;p v&#x1ee5;">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909409767" ID="ID_1130933249" MODIFIED="1453993476146" STYLE="fork" TEXT="Ph&#x1ea5;n &#x111;&#x1ea5;u ho&#xe0;n th&#xe0;nh t&#x1ed1;t c&#xf4;ng vi&#x1ec7;c">
<font NAME="UTM Times" SIZE="20"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1453906941889" ID="ID_487973659" MODIFIED="1453993476147" TEXT="S&#xe1;ng t&#x1ea1;o">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
<node COLOR="#990000" CREATED="1453909424941" ID="ID_1651826362" MODIFIED="1453993476150" STYLE="fork" TEXT="&#x110;&#xe2;y l&#xe0; ch&#xec;a kh&#xf3;a th&#xe0;nh c&#xf4;ng, t&#x1ea1;o n&#xea;n s&#x1ef1; kh&#xe1;c bi&#x1ec7;t">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909433950" ID="ID_1676274308" MODIFIED="1453993476151" STYLE="fork" TEXT="M&#x1ed7;i nh&#xe2;n vi&#xea;n ph&#x1ea3;i lu&#xf4;n lu&#xf4;n l&#xe0;m m&#x1edb;i b&#x1ea3;n th&#xe2;n">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909485790" ID="ID_494166894" MODIFIED="1453993476152" STYLE="fork" TEXT="Ph&#xe1;t huy t&#xed;nh s&#xe1;ng t&#x1ea1;o trong c&#xf4;ng vi&#x1ec7;c">
<font NAME="UTM Times" SIZE="20"/>
</node>
<node COLOR="#990000" CREATED="1453909507249" ID="ID_1145064098" MODIFIED="1453993476153" STYLE="fork">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#272;&#243;ng g&#243;p &#253; ki&#7871;n, t&#236;m ra nh&#7919;ng &#273;i&#7875;m m&#7899;i
    </p>
    <p>
      trong qu&#225; tr&#236;nh x&#226;y d&#7921;ng v&#259;n h&#243;a chung
    </p>
  </body>
</html></richcontent>
<font NAME="UTM Times" SIZE="20"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1453905750982" ID="ID_1925684987" MODIFIED="1453993476158" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n h&#243;a
    </p>
    <p style="text-align: center">
      g&#7917;i email
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<arrowlink COLOR="#ff0099" DESTINATION="ID_868663239" ENDARROW="None" ENDINCLINATION="404;0;" ID="Arrow_ID_24088649" STARTARROW="Default" STARTINCLINATION="404;0;"/>
<font BOLD="true" NAME="UTM Times" SIZE="24"/>
</node>
<node COLOR="#0033ff" CREATED="1453905903333" ID="ID_1340136277" MODIFIED="1453993476163" POSITION="left" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n h&#243;a
    </p>
    <p style="text-align: center">
      h&#7897;i h&#7885;p
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font BOLD="true" NAME="UTM Times" SIZE="24"/>
</node>
<node COLOR="#0033ff" CREATED="1453905922651" ID="ID_1222955773" MODIFIED="1453993476169" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n h&#243;a
    </p>
    <p style="text-align: center">
      g&#243;p &#253;
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font BOLD="true" NAME="UTM Times" SIZE="24"/>
</node>
<node COLOR="#0033ff" CREATED="1453905931574" FOLDED="true" ID="ID_178387873" MODIFIED="1453994152535" POSITION="left" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      C&#244;ng T&#225;c<br />V&#7879; sinh
    </p>
  </body>
</html>
</richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font BOLD="true" NAME="UTM Times" SIZE="24"/>
<node COLOR="#00b439" CREATED="1453906960097" ID="ID_336240373" MODIFIED="1453993476177" TEXT="N&#x1a1;i l&#xe0;m vi&#x1ec7;c">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
</node>
<node COLOR="#00b439" CREATED="1453906967486" ID="ID_630961053" MODIFIED="1453993476179" TEXT="M&#xf4;i tr&#x1b0;&#x1edd;ng l&#xe0;m vi&#x1ec7;c chung">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
</node>
<node COLOR="#00b439" CREATED="1453906975984" ID="ID_718068653" MODIFIED="1453993476181" TEXT="T&#x1ea1;o h&#xec;nh &#x1ea3;nh t&#x1ed1;t n&#x1a1;i c&#xf4;ng s&#x1edf;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="UTM Times" SIZE="22"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1453905941888" ID="ID_1815853956" MODIFIED="1453993476183" POSITION="right" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      V&#259;n h&#243;a
    </p>
    <p style="text-align: center">
      v&#236; c&#7897;ng &#273;&#7891;ng
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font BOLD="true" NAME="UTM Times" SIZE="24"/>
</node>
</node>
</map>
